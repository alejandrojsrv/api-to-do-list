# To Do List API REST

_InfoCasas Full-Stack Challenge Criteria_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Requerimientos 📋

-   Composer (`https://getcomposer.org/download/`)
-   Laravel Lumen (`https://lumen.laravel.com/docs/8.x/installation`)
-   Git (`https://git-scm.com/downloads`)

### Instalación 🔧

Nos ubicamos en cualquier zona en la que desee colocar el proyecto desde la terminal. Clonar el repositorio usando el siguiente comando.

```
git clone https://gitlab.com/alejandroisrv/api-to-do-list
```

## Ejecutamos composer

Ubicados en la raiz del proyecto, por ejemplo: `C:\xampp\htdocs\api-to-do-list` desde la terminal, ejecutamos composer para la instalación de todas las depencias del proyecto.

```
composer install
```

## Variables de entorno

Verifica que al ejecutar `composer install` se haya creado el archivo `.env`, de no ser asi, utilizar el archivo `.env.example` como base. Este archivo contiene las variables de entorno del proyecto, recuerda introducir la información de configuracion correcta (Base de datos, etc...).

## Migraciones

Para crear el modelo de datos ejecutamos lo siguente:

```
php artisan migrate --seed
```

## Pruebas unitarias

Este proyecto fue creado usando TDD (Test-Driven Development) para efectuar los tests ejecuta :

```
php vendor/bin/phpunit
```

## Despliegue 📦

```
php -S localhost:8082 -t public/
```

## Construido con 🛠️

-   [Laravel Lumen](https://lumen.laravel.com/docs/8.x/installation) - El framework del backend usado

## Autor ✒️

-   **Alejandro Sojo**
