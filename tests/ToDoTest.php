<?php

use App\Models\Task;
use Laravel\Lumen\Concerns\RegistersExceptionHandlers;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ToDoTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_tasks_can_be_list()
    {

        Task::factory()->count(10)->create();

        $this->get('/tasks')
            ->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    "id",
                    "name",
                    "completed",
                    "created_at",
                    "updated_at",
                ]
            ]);
    }

    public function test_a_task_can_be_list()
    {

        Task::factory()->count(1)->create();

        $task = Task::first();

        $this->get("/tasks/{$task->id}")
            ->seeStatusCode(200)
            ->seeJsonEquals($task->toArray());
    }

    public function test_a_task_can_be_created()
    {


        $this->post('/tasks', ['name' => 'Tarea 1'])
            ->seeStatusCode(201)
            ->seeJsonStructure([
                "id",
                "name",
                "completed",
                "created_at",
                "updated_at",
            ])
            ->seeJsonContains([
                'name' => 'Tarea 1'
            ])
            ->seeInDatabase('tasks', ['name' => 'Tarea 1']);
    }

    public function test_a_task_can_be_updated()
    {

        Task::factory()->count(10)->create();

        $tasks = Task::all();

        $task = $tasks->first();
        $newName = 'Tarea 12321321';

        $this->put("/tasks/{$task->id}", ['name' => $newName, 'completed' => 1])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                "id",
                "name",
                "completed",
                "created_at",
                "updated_at",
            ])->seeInDatabase('tasks', [
                'id' => $task->id,
                "name" => $newName
            ]);
    }

    public function test_a_task_can_be_deleted()
    {
        Task::factory()->count(10)->create();

        $tasks = Task::all();

        $task = $tasks->first();

        $this->delete("/tasks/{$task->id}")
            ->seeStatusCode(200)
            ->seeJson(['delete' => true])
            ->notSeeInDatabase('tasks', [
                "id" => $task->id
            ]);
    }


    public function test_tasks_can_be_filter_by_name()
    {

        Task::factory()->count(10)->create();

        $tasks = Task::all();

        $this->get('/tasks', ['name' => $tasks->first()->name])
            ->seeStatusCode(200)
            ->seeJsonContains($tasks->first()->toArray());
    }

    public function test_tasks_can_be_filter_by_status()
    {

        Task::factory()->count(10)->create();

        $tasks = Task::get();

        $this->get('/tasks', ['completed' => $tasks->first()->completed])
            ->seeStatusCode(200)
            ->seeJsonContains($tasks->first()->toArray());
    }

    public function test_message_not_found_tasks()
    {

        $this->get('/tasks/3')
            ->seeStatusCode(404)
            ->seeJson([
                'error' => 'Task not found'
            ]);
    }
}
