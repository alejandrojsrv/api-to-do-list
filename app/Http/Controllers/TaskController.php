<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Catch_;

class TaskController extends ApiController
{


    public function getTasks(Request $request)
    {

        try {

            $name = $request->get('name');

            $isComplete = $request->get('completed');

            $tasks = Task::when($name, function ($q) use ($name) {
                return $q->where('name', 'like', "%$name%");
            })->when($isComplete, function ($q) use ($isComplete) {
                return $q->where('completed', $isComplete);
            })
                ->orderBy('created_at', 'DESC')
                ->get();

            return $this->responseJson($tasks);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), $e->getCode());
        }
    }

    public function getTask($id)
    {

        try {

            $task = Task::find($id);

            if (!$task) {
                throw new \Exception("Task not found", 404);
            }

            return $this->responseJson($task);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), $e->getCode());
        }
    }

    public function createTask(Request $request)
    {
        try {

            $this->validate($request, [
                'name' => 'required'
            ]);

            $task = Task::create([
                'name' => $request->get('name'),
                'completed' => 0
            ]);

            return $this->responseJson($task, 201);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), $e->getCode());
        }
    }

    public function updateTask(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'name' => 'string|required',
                'completed' => 'boolean|required'
            ]);

            $task = Task::find($id);

            if (!$task) {
                throw new \Exception("Task not found", 404);
            }

            $task->name = $request->get('name');
            $task->completed = $request->get('completed');
            $task->save();

            return $this->responseJson($task);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), $e->getCode());
        }
    }

    public function deleteTask($id)
    {
        try {

            $task = Task::find($id);

            if (!$task) {
                throw new \Exception("Task not found", 404);
            }

            $task->delete();

            return $this->responseJson(['delete' => true]);
        } catch (\Exception $e) {
            return $this->responseWithError($e->getMessage(), $e->getCode());
        }
    }
}
