<?php

namespace App\Http\Controllers;



class ApiController extends Controller
{


    public function responseJson($data, $code = 200)
    {
        return response()->json($data, $code);
    }

    public function responseWithError($message, $code = 422)
    {
        return response()->json(['error' => $message], !$code ? 422 : $code);
    }
}
